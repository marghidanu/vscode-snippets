#!/usr/bin/env perl

use strict;
use warnings;

use File::Find::Rule;
use File::Slurp qw(read_file);
use File::Basename;

use JSON::XS;

use Text::FrontMatter::YAML;

unless(scalar(@ARGV)) {
    warn("Usage: ${0} [dir1] [dir2]\n");
    exit(1);
}

my @files = File::Find::Rule->file()
    ->name('*.txt')
    ->not_empty()
    ->in(@ARGV);

my $result = {};
foreach my $file (@files) {
    my $name = basename($file, '.txt');

    my $text = read_file($file, {binmode => ':utf8'});
    my $tfm = Text::FrontMatter::YAML->new(
        document_string => $text,
    );

    my $meta = $tfm->frontmatter_hashref();
    my $body = $tfm->data_text();

    $result->{$name} = {
        prefix => $meta->{prefix} || '',
        description => $meta->{description} || $name,
        body => [ split(/\n/, $body) ],
    };
}

unless(scalar(keys(%{$result}))) {
    warn("No file was added. Exiting...\n");
    exit(1);
}

my $output = JSON::XS->new()
    ->utf8()
    ->pretty()
    ->canonical(1)
    ->encode($result);

printf("// Generated at %s by %s\n\n%s",
    my $str = localtime(),
    basename($0),
    $output,
);

__END__