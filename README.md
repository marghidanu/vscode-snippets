# vscode-generator

## Description

Custom Visual Studio Code snippets generator.

## Development environment

The development environment is available via Docker and docker-compose. Simply run the commands below, you should get some output.

    docker-compose build
    docker-compose run --rm generator

**NOTE:** After updating the snippets don't forget to generate a new destribution in the local filesystem, the generated files will also be checked in.

    docker-compose run --rm generator | tee dist/perl.json

## Installation

In case you want to deploy outside of Docker just run the following commands:

    curl -sL http://cpanmin.us | - -nq --installdeps .
    ./bin/generator.pl snippets/ > snippets.json

## Snippet format

Snippets can be stored in text files with an attached FrontMatter header for additonal metadata. 

```yaml
    ---
    prefix:
        - "tab_prefix"
        - "etc.."
    description: "A description for your snippet"
    ---
    program ${1:ProgramName};
    begin
        ${0:// Add code here ...}
    end
```

* **prefix**: A list orstring that will be used for "tab completion".
* **description**: This is optional, is missing it will default to the file name.

The generator looks for text files recursively so you can organize the files as you like.

## Resources

For additional information on how to create snippets please see https://code.visualstudio.com/docs/editor/userdefinedsnippets