FROM perl:5.30

WORKDIR /opt/app

COPY cpanfile .
RUN cpanm -nq --installdeps .

COPY . .

CMD perl bin/generator.pl snippets/